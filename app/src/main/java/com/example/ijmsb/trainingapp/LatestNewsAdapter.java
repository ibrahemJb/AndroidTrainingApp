package com.example.ijmsb.trainingapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class LatestNewsAdapter extends RecyclerView.Adapter<LatestNewsAdapter.ViewHolder> {
    private List<NewsListItem> listItems;
    private Context context;

    public LatestNewsAdapter(List<NewsListItem> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_item,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final NewsListItem listItem = listItems.get(position);
        holder.textViewHead.setText(listItem.getHead());
        Picasso.get()
                .load(listItem.getImageUrl())
                .resize(200,200)
                .into(holder.imageView);
        holder.viewMore.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(context,NewsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("Object",listItem.getJsonObject().toString());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends  RecyclerView.ViewHolder{
        public TextView textViewHead;
        public ImageView imageView;
        public TextView viewMore;

        public ViewHolder(View itemView) {
            super(itemView);
            textViewHead = (TextView) itemView.findViewById(R.id.latestNewsText);
            imageView = (ImageView) itemView.findViewById(R.id.latestNewsImage);
            viewMore = (TextView) itemView.findViewById(R.id.viewMore);
        }
    }
}
