package com.example.ijmsb.trainingapp;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.mikepenz.materialdrawer.DrawerBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.zip.Inflater;

import dmax.dialog.SpotsDialog;

public class MainActivity extends AppCompatActivity implements ScrollViewListener {
    private String mostWatchedString = new String("[{\"ID\":\"2007\",\"Title\":\"Yuuki Yuuna wa Yuusha de Aru: Washio Sumi no Shou\",\"State\":\"Completed\",\"Year\":\"2017\",\"Rate\":\"7.43\",\"img\":\"https:\\/\\/myanimelist.cdn-dena.com\\/images\\/anime\\/7\\/83012.jpg\",\"Gen\":\"[\\\"Drama\\\",\\\"Fantasy\\\",\\\"Magic\\\",\\\"Slice of Life\\\"]\",\"time\":\",-Yuuki Yuuna...Shou SP\",\"type\":\"0\",\"allow\":\"1\",\"popu\":\"0\",\"Rating\":\"13+\",\"animekay\":\"wfja8uyqrscle9xgozidvpbkthnmn\"},{\"ID\":\"2024\",\"Title\":\"ClassicaLoid 2nd Season\",\"State\":\"Ongoing\",\"Year\":\"2017\",\"Rate\":\"6.76\",\"img\":\"https:\\/\\/myanimelist.cdn-dena.com\\/images\\/anime\\/2\\/88678.jpg\",\"Gen\":\"[\\\"Music\\\",\\\"Comedy\\\"]\",\"time\":\",- ClassicaLoid 2nd Season 05\",\"type\":\"0\",\"allow\":\"1\",\"popu\":\"0\",\"Rating\":\"13+\",\"animekay\":\"wfja8uyqrscle9xgozidvpbkthnmn\"},{\"ID\":\"2027\",\"Title\":\"Kubikiri Cycle: Aoiro Savant to Zaregototsukai\",\"State\":\"Completed\",\"Year\":\"2016\",\"Rate\":\"7.96\",\"img\":\"https:\\/\\/myanimelist.cdn-dena.com\\/images\\/anime\\/12\\/81588.jpg\",\"Gen\":\"[\\\"Action\\\",\\\"Mystery\\\",\\\"Supernatural\\\",\\\"Drama\\\"]\",\"time\":\",- Kubikiri Cycle 1 - 8 End\",\"type\":\"1\",\"allow\":\"1\",\"popu\":\"0\",\"Rating\":\"17+\",\"animekay\":\"wfja8uyqrscle9xgozidvpbkthnmn\"},{\"ID\":\"2026\",\"Title\":\"Infini-T Force\",\"State\":\"Ongoing\",\"Year\":\"2017\",\"Rate\":\"6.42\",\"img\":\"https:\\/\\/myanimelist.cdn-dena.com\\/images\\/anime\\/6\\/88430.jpg\",\"Gen\":\"[\\\"Action\\\",\\\"Super Power\\\",\\\"Seinen\\\"]\",\"time\":\",- Infini-T Force 01\",\"type\":\"0\",\"allow\":\"1\",\"popu\":\"0\",\"Rating\":\"13+\",\"animekay\":\"wfja8uyqrscle9xgozidvpbkthnmn\"},{\"ID\":\"2025\",\"Title\":\"Love Kome: We Love Rice 2nd Season\",\"State\":\"Ongoing\",\"Year\":\"2017\",\"Rate\":\"5.03\",\"img\":\"https:\\/\\/myanimelist.cdn-dena.com\\/images\\/anime\\/4\\/88115.jpg\",\"Gen\":\"[\\\"Slice of Life\\\"]\",\"time\":\",- Love Kome...Rice S2 02\",\"type\":\"0\",\"allow\":\"1\",\"popu\":\"0\",\"Rating\":\"13+\",\"animekay\":\"wfja8uyqrscle9xgozidvpbkthnmn\"},{\"ID\":\"2010\",\"Title\":\"Mahoutsukai no Yome: Hoshi Matsu Hito\",\"State\":\"Completed\",\"Year\":\"2016\",\"Rate\":\"8.23\",\"img\":\"https:\\/\\/myanimelist.cdn-dena.com\\/images\\/anime\\/4\\/80587.jpg\",\"Gen\":\"[\\\"Magic\\\",\\\"Fantasy\\\"]\",\"time\":\",-Mahoutsukai no Yome OVA 1 - 3\",\"type\":\"0\",\"allow\":\"1\",\"popu\":\"0\",\"Rating\":\"13+\",\"animekay\":\"wfja8uyqrscle9xgozidvpbkthnmn\"},{\"ID\":\"1943\",\"Title\":\"18if\",\"State\":\"Ongoing\",\"Year\":\"2017\",\"Rate\":\"5.78\",\"img\":\"https:\\/\\/myanimelist.cdn-dena.com\\/images\\/anime\\/7\\/86743.jpg\",\"Gen\":\"[\\\"Supernatural\\\"]\",\"time\":\",- 18if 08\",\"type\":\"0\",\"allow\":\"1\",\"popu\":\"0\",\"Rating\":\"13+\",\"animekay\":\"wfja8uyqrscle9xgozidvpbkthnmn\"},{\"ID\":\"1969\",\"Title\":\"Gundam Build Fighters: Battlogue\",\"State\":\"Ongoing\",\"Year\":\"2017\",\"Rate\":\"7.48\",\"img\":\"https:\\/\\/myanimelist.cdn-dena.com\\/images\\/anime\\/9\\/87160.jpg\",\"Gen\":\"[\\\"Action\\\",\\\"Sci-Fi\\\",\\\"Mecha\\\"]\",\"time\":\",- Gundam Build...Battlogue 01\",\"type\":\"0\",\"allow\":\"1\",\"popu\":\"0\",\"Rating\":\"13+\",\"animekay\":\"wfja8uyqrscle9xgozidvpbkthnmn\"},{\"ID\":\"1777\",\"Title\":\"Nobunaga no Shinobi\",\"State\":\"Ongoing\",\"Year\":\"2016\",\"Rate\":\"6.64\",\"img\":\"https:\\/\\/myanimelist.cdn-dena.com\\/images\\/anime\\/2\\/78715.jpg\",\"Gen\":\"[\\\"Comedy\\\",\\\"Historical\\\"]\",\"time\":\",- Nobunaga no Shinobi 04\",\"type\":\"0\",\"allow\":\"1\",\"popu\":\"0\",\"Rating\":\"\",\"animekay\":\"wfja8uyqrscle9xgozidvpbkthnmn\"},{\"ID\":\"1778\",\"Title\":\"Monster Hunter Stories: Ride On\",\"State\":\"Ongoing\",\"Year\":\"2016\",\"Rate\":\"5.91\",\"img\":\"https:\\/\\/myanimelist.cdn-dena.com\\/images\\/anime\\/7\\/76232.jpg\",\"Gen\":\"[\\\"Fantasy\\\"]\",\"time\":\",- Monster Hunter...Ride On 03\",\"type\":\"0\",\"allow\":\"1\",\"popu\":\"0\",\"Rating\":\"13+\",\"animekay\":\"wfja8uyqrscle9xgozidvpbkthnmn\"}]");
    private static final String URL_DATA = "https://reslayer.com/app/NewGet.php?fu=1";
    private String newsUrl ="https://animeslayer.com/Data/GetNews_LM.php?";
    private int s = 0;
    private int e = 25;
    private RecyclerView recyclerView;
    private RecyclerView mostWatchedRecyclerView;
    private RecyclerView latestNewsRecyclerView;
    private RecyclerView favouritesRecyclerView;

    private RecyclerView.Adapter adapter;
    private RecyclerView.Adapter mostWatchedAdapter;
    private RecyclerView.Adapter latestNewsAdapter;
    private RecyclerView.Adapter favouritesAdapter;

    private List<ListItem> listItems;
    private List<ListItem> mostWatchedList;
    private List<NewsListItem> latestNewsList;
    private List<ListItem> favouritesList;
    private SpotsDialog dialog;
    public static final String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";
    DBHelper myDB;
    LayoutParams layoutparams;

    private DrawerLayout dl;
    private ActionBarDrawerToggle t;
    private NavigationView nv;
    private SwipeRefreshLayout swipeContainer;


    static final int PICK_CONTACT_REQUEST = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadRecyclerViewData();
                swipeContainer.setRefreshing(false);
            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        Intent ll24 = new Intent(this, AlarmReceiverNotification.class);
        PendingIntent recurringLl24 = PendingIntent.getBroadcast(this, 0, ll24, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarms = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarms.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 600000, recurringLl24);


        dl = (DrawerLayout)findViewById(R.id.activity_main_drawer);
        t = new ActionBarDrawerToggle(this, dl,R.string.Open, R.string.Close);
        dl.addDrawerListener(t);
        t.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        nv = (NavigationView)findViewById(R.id.nv);
        TextView textView =(TextView) nv.getHeaderView(0).findViewById(R.id.emailTextView);
        SharedPreferences prefs = getSharedPreferences("MyPreff",MODE_PRIVATE);
        String RestoredText = prefs.getString("email",null);
        textView.setText(RestoredText);
        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch(id)
                {
                    case R.id.logout:
                        logout();
                    default:
                        return true;
                }
            }
        });
        myDB = new DBHelper(this);
        dialog = new SpotsDialog(this);
        recyclerView = (RecyclerView) findViewById(R.id.new_episode_recycler_view);
        mostWatchedRecyclerView = (RecyclerView) findViewById(R.id.mostWatchedRecyclerView);
        latestNewsRecyclerView = (RecyclerView) findViewById(R.id.latestNewsRecyclerView);
        favouritesRecyclerView = (RecyclerView) findViewById(R.id.favouritesRecyclerView);

        ObservableScrollView observableScrollView = (ObservableScrollView) findViewById(R.id.mainScrollView);
        observableScrollView.setScrollViewListener(this);

        recyclerView.setHasFixedSize(true);
        mostWatchedRecyclerView.setHasFixedSize(true);
        latestNewsRecyclerView.setHasFixedSize(true);
        favouritesRecyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager mostWatchedlayoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager favouritesLayoutManeger =
                new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);

        recyclerView.setLayoutManager(layoutManager);
        mostWatchedRecyclerView.setLayoutManager(mostWatchedlayoutManager);
        latestNewsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        favouritesRecyclerView.setLayoutManager(favouritesLayoutManeger);

        listItems = new ArrayList<>();
        mostWatchedList = new ArrayList<>();
        latestNewsList = new ArrayList<>();
        favouritesList = new ArrayList<>();
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.logoutClick);
        linearLayout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                logout();
            }
        });

        loadRecyclerViewData();
    }
    private void loadRecyclerViewData(){
        dialog.show();
        listItems = new ArrayList<>();
        mostWatchedList = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_DATA, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONArray array = new JSONArray(response);
                    for(int i=0;i<array.length();i++){
                        JSONObject obj = array.getJSONObject(i);
                        ListItem item = new ListItem(obj);
                        listItems.add(item);
                    }
                    adapter = new NewEpisodeAdapter(listItems,MainActivity.this);
                    recyclerView.setAdapter(adapter);
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        try{
            JSONArray mostWatched = new JSONArray(mostWatchedString);
            for(int i=0;i<mostWatched.length();i++){
                JSONObject obj = mostWatched.getJSONObject(i);
                ListItem item = new ListItem(obj);
                mostWatchedList.add(item);
            }
            mostWatchedAdapter = new NewEpisodeAdapter(mostWatchedList,MainActivity.this);
            mostWatchedRecyclerView.setAdapter(mostWatchedAdapter);

        }
        catch (JSONException je){
            je.printStackTrace();
        }
        loadFavourites();
        s=0;
        e=25;
        loadNews();
    }
    public void loadMoreNews(View view){
        loadNews();
    }
    public void loadFavourites(){
        try{
            favouritesList = new ArrayList<>();
            ArrayList<String> favourites = myDB.getAllFavourites();
            for(int i =0;i<favourites.size();i++){
                JSONObject obj = new JSONObject(favourites.get(i));
                ListItem item = new ListItem(obj);
                favouritesList.add(item);
            }
            favouritesAdapter = new NewEpisodeAdapter(favouritesList,MainActivity.this);
            favouritesRecyclerView.setAdapter(favouritesAdapter);
        }catch (JSONException je){
            je.printStackTrace();
        }
    }
    public void loadNews()
    {
        String newsURL = newsUrl+"s="+s+"&e="+e;

        StringRequest LatestNewsStringRequest = new StringRequest(Request.Method.GET, newsURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    dialog.dismiss();
                    latestNewsList = new ArrayList<>();
                    JSONArray array = new JSONArray(response);
                    for(int i=1;i<array.length();i++){
                        JSONObject obj = array.getJSONObject(i);
                        NewsListItem item = new NewsListItem(obj);
                        latestNewsList.add(item);
                    }
                    latestNewsAdapter = new LatestNewsAdapter(latestNewsList,MainActivity.this);
                    latestNewsRecyclerView.setAdapter(latestNewsAdapter);
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
        RequestQueue LatestNewsRequestQueue = Volley.newRequestQueue(this);
        LatestNewsRequestQueue.add(LatestNewsStringRequest);
        s+=25;
        e+=25;
    }
    @Override
    public void onBackPressed()
    {
        if(dl.isDrawerOpen(GravityCompat.START)){
            dl.closeDrawer(GravityCompat.START);
        }
        else{
            this.finish();
        }
    }

    @Override
    public void onScrollEnded(ObservableScrollView scrollView, int x, int y, int oldx, int oldy) {
        dialog.show();
        loadNews();
    }
    @Override
    public void onResume()
    {  // After a pause OR at startup
        super.onResume();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(t.onOptionsItemSelected(item))
            return true;

        return super.onOptionsItemSelected(item);
    }
    public void logout(){
        new AlertDialog.Builder(this)
                .setTitle("Alert")
                .setMessage("Do you really want to logout?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        getSharedPreferences("MyPreff",MODE_PRIVATE).edit().remove("email").apply();
                        Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }})
                .setNegativeButton(android.R.string.no, null).show();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                loadFavourites();
            }
            if (resultCode == Activity.RESULT_CANCELED) {

            }
        }
    }
    @Override
    public boolean onCreateOptionsMenu( Menu menu ) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_search,menu);
        MenuItem menuItem = menu.findItem(R.id.menuSearch);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(newText.equals("")){
                    //mostWatchedAdapter = new NewEpisodeAdapter(mostWatchedList,MainActivity.this);
                    mostWatchedRecyclerView.setAdapter(mostWatchedAdapter);
                    //favouritesAdapter = new NewEpisodeAdapter(favouritesList,MainActivity.this);
                    favouritesRecyclerView.setAdapter(favouritesAdapter);
                    //latestNewsAdapter = new LatestNewsAdapter(latestNewsList,MainActivity.this);
                    latestNewsRecyclerView.setAdapter(latestNewsAdapter);
                   // adapter = new NewEpisodeAdapter(listItems,MainActivity.this);
                    recyclerView.setAdapter(adapter);
                    return true;
                }
                RecyclerView.Adapter newmostWatchedAdapter;
                RecyclerView.Adapter newfavouritesAdapter;
                RecyclerView.Adapter newlatestNewsAdapter;
                RecyclerView.Adapter newadapter;

                List<ListItem> newlistItems = new ArrayList<>();
                List<ListItem> newmostWatchedList = new ArrayList<>();
                List<NewsListItem> newlatestNewsList = new ArrayList<>();
                List<ListItem> newfavouritesList = new ArrayList<>();
                for(int i =0;i<listItems.size();i++){
                    if(listItems.get(i).getHead().toLowerCase().contains(newText.toLowerCase())){
                        newlistItems.add(listItems.get(i));
                    }
                }
                for(int i =0;i<mostWatchedList.size();i++){
                    if(mostWatchedList.get(i).getHead().toLowerCase().contains(newText.toLowerCase())){
                        newmostWatchedList.add(listItems.get(i));
                    }
                }
                for(int i =0;i<favouritesList.size();i++){
                    if(favouritesList.get(i).getHead().toLowerCase().contains(newText.toLowerCase())){
                        newfavouritesList.add(favouritesList.get(i));
                    }
                }
                for(int i =0;i<latestNewsList.size();i++){
                    if(latestNewsList.get(i).getHead().toLowerCase().contains(newText.toLowerCase())){
                        newlatestNewsList.add(latestNewsList.get(i));
                    }
                }
                newmostWatchedAdapter = new NewEpisodeAdapter(newmostWatchedList,MainActivity.this);
                mostWatchedRecyclerView.setAdapter(newmostWatchedAdapter);
                newfavouritesAdapter = new NewEpisodeAdapter(newfavouritesList,MainActivity.this);
                favouritesRecyclerView.setAdapter(newfavouritesAdapter);
                newlatestNewsAdapter = new LatestNewsAdapter(newlatestNewsList,MainActivity.this);
                latestNewsRecyclerView.setAdapter(newlatestNewsAdapter);
                newadapter = new NewEpisodeAdapter(newlistItems,MainActivity.this);
                recyclerView.setAdapter(newadapter);
                return true;
            }
        });

       return super.onCreateOptionsMenu(menu);
    }
}

