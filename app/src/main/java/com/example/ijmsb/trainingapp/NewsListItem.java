package com.example.ijmsb.trainingapp;

import org.json.JSONException;
import org.json.JSONObject;

public class NewsListItem {
    private String head;
    private String imageUrl;
    private JSONObject jsonObject;
    public NewsListItem(JSONObject obj) {
        try{
            this.jsonObject = obj;
            this.head = obj.getString("title");
            this.imageUrl = obj.getString("img");
        }catch (JSONException je){
            je.printStackTrace();
        }
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public String getHead() {
        return head;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
