package com.example.ijmsb.trainingapp;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RSSPullService extends IntentService {
    private String newEpisodesString = new String("[{\"ID\":\"2117\",\"Title\":\"Inazuma Eleven: Ares no Tenbin\",\"State\":\"Ongoing\",\"Year\":\"2018\",\"Rate\":\"7.63\",\"img\":\"https:\\/\\/myanimelist.cdn-dena.com\\/images\\/anime\\/1188\\/90223.jpg\",\"Gen\":\"[\\\"Sports\\\"]\",\"time\":\",- Inazuma Eleven...Tenbin 09 END\",\"type\":\"0\",\"allow\":\"1\",\"popu\":\"0\",\"Rating\":\"\",\"animekay\":\"wfja8uyqrscle9xgozidvpbkthnmn\"},{\"ID\":\"2044\",\"Title\":\"Toji no Miko\",\"State\":\"Ongoing\",\"Year\":\"2018\",\"Rate\":\"7.02\",\"img\":\"https:\\/\\/myanimelist.cdn-dena.com\\/images\\/anime\\/13\\/89885.jpg\",\"Gen\":\"[\\\"Action\\\",\\\"Fantasy\\\"]\",\"time\":\",- Toji no Miko 21\",\"type\":\"0\",\"allow\":\"1\",\"popu\":\"0\",\"Rating\":\"\",\"animekay\":\"wfja8uyqrscle9xgozidvpbkthnmn\"},{\"ID\":\"2107\",\"Title\":\"Hinamatsuri\",\"State\":\"Ongoing\",\"Year\":\"2018\",\"Rate\":\"7.70\",\"img\":\"https:\\/\\/myanimelist.cdn-dena.com\\/images\\/anime\\/1943\\/91370.jpg\",\"Gen\":\"[\\\"Sci-Fi\\\",\\\"Slice of Life\\\",\\\"Comedy\\\",\\\"Supernatural\\\",\\\"Seinen\\\"]\",\"time\":\",- Hinamatsuri 09\",\"type\":\"0\",\"allow\":\"1\",\"popu\":\"0\",\"Rating\":\"\",\"animekay\":\"wfja8uyqrscle9xgozidvpbkthnmn\"},{\"ID\":\"2065\",\"Title\":\"Hakyuu Houshin Engi\",\"State\":\"Ongoing\",\"Year\":\"2018\",\"Rate\":\"6.95\",\"img\":\"https:\\/\\/myanimelist.cdn-dena.com\\/images\\/anime\\/11\\/88796.jpg\",\"Gen\":\"[\\\"Adventure\\\",\\\"Demons\\\",\\\"Supernatural\\\",\\\"Fantasy\\\",\\\"Shounen\\\"]\",\"time\":\",- Hakyuu Houshin Engi 19\",\"type\":\"0\",\"allow\":\"1\",\"popu\":\"0\",\"Rating\":\"\",\"animekay\":\"wfja8uyqrscle9xgozidvpbkthnmn\"},{\"ID\":\"2100\",\"Title\":\"Lostorage Conflated WIXOSS\",\"State\":\"Ongoing\",\"Year\":\"2018\",\"Rate\":\"7.03\",\"img\":\"https:\\/\\/myanimelist.cdn-dena.com\\/images\\/anime\\/1558\\/90899.jpg\",\"Gen\":\"[\\\"Game\\\",\\\"Psychological\\\"]\",\"time\":\",- Lostorage Conflated WIXOSS 09\",\"type\":\"1\",\"allow\":\"1\",\"popu\":\"0\",\"Rating\":\"17+\",\"animekay\":\"wfja8uyqrscle9xgozidvpbkthnmn\"},{\"ID\":\"2095\",\"Title\":\"Comic Girls\",\"State\":\"Ongoing\",\"Year\":\"2018\",\"Rate\":\"7.29\",\"img\":\"https:\\/\\/myanimelist.cdn-dena.com\\/images\\/anime\\/1148\\/90325.jpg\",\"Gen\":\"[\\\"Comedy\\\",\\\"Slice of Life\\\"]\",\"time\":\",- Comic Girls 09\",\"type\":\"0\",\"allow\":\"1\",\"popu\":\"0\",\"Rating\":\"\",\"animekay\":\"wfja8uyqrscle9xgozidvpbkthnmn\"},{\"ID\":\"2120\",\"Title\":\"Wotaku ni Koi wa Muzukashii\",\"State\":\"Ongoing\",\"Year\":\"2018\",\"Rate\":\"7.91\",\"img\":\"https:\\/\\/myanimelist.cdn-dena.com\\/images\\/anime\\/1545\\/90922.jpg\",\"Gen\":\"[\\\"Comedy\\\",\\\"Romance\\\"]\",\"time\":\",- Wotaku ni...Muzukashii 08\",\"type\":\"0\",\"allow\":\"1\",\"popu\":\"0\",\"Rating\":\"\",\"animekay\":\"wfja8uyqrscle9xgozidvpbkthnmn\"},{\"ID\":\"2093\",\"Title\":\"Tada-kun wa Koi wo Shinai\",\"State\":\"Ongoing\",\"Year\":\"2018\",\"Rate\":\"7.46\",\"img\":\"https:\\/\\/myanimelist.cdn-dena.com\\/images\\/anime\\/1446\\/91841.jpg\",\"Gen\":\"[\\\"Comedy\\\",\\\"Romance\\\"]\",\"time\":\",- Tada-kun...Shinai 09\",\"type\":\"0\",\"allow\":\"1\",\"popu\":\"0\",\"Rating\":\"\",\"animekay\":\"wfja8uyqrscle9xgozidvpbkthnmn\"}]");
    private List<ListItem> favouritesList;
    private static final String URL_DATA = "https://reslayer.com/app/NewGet.php?fu=1";
    DBHelper myDB;
    private List<ListItem> listItems;
    public RSSPullService() {
        super("test-service");
    }
    @Override
    protected void onHandleIntent(Intent intent) {
        myDB = new DBHelper(this);
        listItems = new ArrayList<>();
        try{
            favouritesList = new ArrayList<>();
            ArrayList<String> favourites = myDB.getAllFavourites();
            for(int i =0;i<favourites.size();i++){
                JSONObject obj = new JSONObject(favourites.get(i));
                ListItem item = new ListItem(obj);
                favouritesList.add(item);
            }
        }catch (JSONException je){
            je.printStackTrace();
        }
        /*StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_DATA, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONArray array = new JSONArray(response);
                    for(int i=0;i<array.length();i++){
                        JSONObject obj = array.getJSONObject(i);
                        ListItem item = new ListItem(obj);
                        listItems.add(item);
                    }
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);*/
        try{
            JSONArray array = new JSONArray(newEpisodesString);
            for(int i=0;i<array.length();i++){
                JSONObject obj = array.getJSONObject(i);
                ListItem item = new ListItem(obj);
                listItems.add(item);
            }
        }
        catch (JSONException e){
            e.printStackTrace();
        }


        ImageView imageView = new ImageView(this);
        for(int i=0;i<favouritesList.size();i++){
            for(int j=0;j<listItems.size();j++){
                if(favouritesList.get(i).getId() == listItems.get(j).getId()){
                    if(favouritesList.get(i).getTime().compareTo(listItems.get(j).getTime())<0){
                        Intent resultIntent = new Intent(this, AnimePageActivity.class);
                        resultIntent.putExtra("animeObject",listItems.get(j).getJsonObject().toString());
                        resultIntent.putExtra("close",0);
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        stackBuilder.addParentStack(AnimePageActivity.class);
                        stackBuilder.addNextIntent(resultIntent);
                        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
                        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this,"M_CH_ID");
                        mBuilder.setSmallIcon(R.drawable.ic_launcher_background);
                        mBuilder.setContentTitle(listItems.get(j).getHead());
                        mBuilder.setContentText(listItems.get(j).getTime());
                        mBuilder.setDefaults(Notification.DEFAULT_ALL);
                        mBuilder.setAutoCancel(true);
                        mBuilder.setContentIntent(resultPendingIntent);
                        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        mNotificationManager.notify(listItems.get(j).getId(), mBuilder.build());
                        myDB.updateFavourite(favouritesList.get(i).getId(),listItems.get(j).getJsonObject().toString());
                    }
                }
            }
        }
    }
}