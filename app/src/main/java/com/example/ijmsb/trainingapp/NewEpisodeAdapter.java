package com.example.ijmsb.trainingapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.List;

public class NewEpisodeAdapter extends RecyclerView.Adapter<NewEpisodeAdapter.ViewHolder> {
    private List<ListItem> listItems;
    private Context context;
    private final Transformation transformation = new RoundedTransformationBuilder()
            .cornerRadiusDp(6)
            .oval(false)
            .build();

    public NewEpisodeAdapter(List<ListItem> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }

    @NonNull
    @Override

    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final ListItem listItem = listItems.get(position);
        holder.textViewHead.setText(listItem.getHead());
        Picasso.get()
                .load(listItem.getImageUrl())
                .resize(225,300)
                .transform(transformation)
                .into(holder.imageView);
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent(context,AnimePageActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("animeObject",listItem.getJsonObject().toString());
                context.startActivity(intent);*/
                new openActivity(listItem.getJsonObject().toString());
            }
        });
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textViewHead;
        public ImageView imageView;
        public LinearLayout linearLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            textViewHead = (TextView) itemView.findViewById(R.id.textViewHead);
            imageView = (ImageView) itemView.findViewById(R.id.NewEpisodeImageView);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.itemContainer);
        }
    }
    public class openActivity extends Activity{
        static final int PICK_CONTACT_REQUEST = 1;
        public openActivity(String animeObject){
            Intent intent = new Intent(context,AnimePageActivity.class);
            intent.putExtra("animeObject",animeObject);
            ((Activity) context).startActivityForResult(intent,PICK_CONTACT_REQUEST);
        }
    }
}
