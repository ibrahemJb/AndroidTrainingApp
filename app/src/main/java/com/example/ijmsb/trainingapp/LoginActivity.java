package com.example.ijmsb.trainingapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.regex.Pattern;

public class LoginActivity extends AppCompatActivity {
    private EditText inputEmail, inputPassword;
    private TextInputLayout inputLayoutName, inputLayoutEmail, inputLayoutPassword;
    private Button btnLogin;
    private DBHelper mydb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
        mydb = new DBHelper(this);
        SharedPreferences prefs = getSharedPreferences("MyPreff", MODE_PRIVATE);
        String restoredText = prefs.getString("email", null);
        if (restoredText != null) {
            String email = prefs.getString("email", "No name defined");
            if(Pattern.compile("^\\D.*@\\D.*\\.\\D.*$").matcher(email).matches()) {
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                this.finish();
            }
        }
        inputLayoutEmail = (TextInputLayout) findViewById(R.id.input_layout_email);
        inputLayoutPassword = (TextInputLayout) findViewById(R.id.input_layout_password);
        inputEmail = (EditText) findViewById(R.id.input_email);
        inputPassword = (EditText) findViewById(R.id.input_password);
        btnLogin = (Button) findViewById(R.id.loginButton);
        inputEmail.addTextChangedListener(new MyTextWatcher(inputEmail));
        final TextView tv=(TextView)findViewById(R.id.forgotPass);

        tv.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                tv.setText("YOU ROCK!");
            }
        });
    }
    public void login(View view){
        if (!validateEmail()) {
            return;
        }
        SharedPreferences.Editor editor = getSharedPreferences("MyPreff", MODE_PRIVATE).edit();
        editor.putString("email", inputEmail.getText().toString().trim());
        editor.apply();
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
        this.finish();
        //overridePendingTransition  (R.anim.right_slide_in, R.anim.right_slide_out);
    }
    private boolean validateEmail() {
        String email = inputEmail.getText().toString().trim();
        if (email.isEmpty() || !isValidEmail(email)) {
            inputLayoutEmail.setError(getString(R.string.err_msg_email));
            requestFocus(inputEmail);
            return false;
        } else {
            inputLayoutEmail.setErrorEnabled(false);
        }

        return true;
    }
    private static boolean isValidEmail(String email) {
        boolean isMatch = Pattern.compile("^\\D.*@\\D.*\\.\\D.*$").matcher(email).matches();
        return !TextUtils.isEmpty(email) && isMatch;
    }
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
    private class MyTextWatcher implements TextWatcher {
        private View view;
        private MyTextWatcher(View view) {
            this.view = view;
        }
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }
        public void afterTextChanged(Editable editable) {
            validateEmail();
        }
    }
}

