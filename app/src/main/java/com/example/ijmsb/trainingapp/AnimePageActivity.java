package com.example.ijmsb.trainingapp;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.NavigationMenuItemView;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

public class AnimePageActivity extends AppCompatActivity {
    public JSONObject jsonObject;
    private int ItemState =0;
    private int ID=0;
    DBHelper myDB;
    FloatingActionButton fab;
    private Intent returnIntent;
    private int changedState = 0;
    private static final int RSS_JOB_ID = 1000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anime_page);
        returnIntent = new Intent();
        String animeObject = getIntent().getStringExtra("animeObject");
        myDB = new DBHelper(this);
        Cursor res = null;
        try{
            jsonObject = new JSONObject(animeObject);
            getSupportActionBar().setTitle(jsonObject.get("Title").toString());
            ID = jsonObject.getInt("ID");
            res = myDB.getData(ID);
        }catch (JSONException je){je.printStackTrace(); }
        fab = findViewById(R.id.fab);
        if((res != null) && (res.getCount() > 0)){
            ItemState = 1;
            fab.setImageResource(R.drawable.ic_star_black_24dp);
        }
        changedState = ItemState;
        try{
            ((TextView) findViewById(R.id.animeTextViewTitle)).setText(jsonObject.get("Title").toString());
            Picasso.get()
                    .load(jsonObject.get("img").toString())
                    .into((ImageView) findViewById(R.id.animeImageView));
        }catch (JSONException je){
            je.printStackTrace();
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ItemState == 0){
                    fab.setImageResource(R.drawable.ic_star_black_24dp);
                    ItemState = 1;
                    myDB.insertFavourite(ID,jsonObject.toString());
                    Toast.makeText(AnimePageActivity.this, "Added to Favourites List",
                            Toast.LENGTH_LONG).show();
                }
                else{
                    fab.setImageResource(R.drawable.ic_star_border_black_24dp);
                    ItemState = 0;
                    myDB.deleteFavourite(ID);
                    Toast.makeText(AnimePageActivity.this, "Removed From Favourites List",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    @Override
    @TargetApi(16)
    public void onBackPressed()
    {
        overridePendingTransition  (R.anim.right_slide_in, R.anim.right_slide_out);
        if(getIntent().getIntExtra("close",1)==0){
            this.finishAffinity();
        }
        if(changedState == ItemState){
            setResult(Activity.RESULT_CANCELED,returnIntent);
        }
        else{
            returnIntent.putExtra("result","1");
            setResult(Activity.RESULT_OK,returnIntent);
        }
        this.finish();
    }
    @Override
    @TargetApi(16)
    public boolean onOptionsItemSelected(MenuItem item) {
        if(getIntent().getIntExtra("close",1)==0){
            this.finishAffinity();
        }
        overridePendingTransition  (R.anim.right_slide_in, R.anim.right_slide_out);
        if(changedState == ItemState){
            setResult(Activity.RESULT_CANCELED,returnIntent);
        }
        else{
            returnIntent.putExtra("result","1");
            setResult(Activity.RESULT_OK,returnIntent);
        }
        this.finish();
        return true;
    }

}
