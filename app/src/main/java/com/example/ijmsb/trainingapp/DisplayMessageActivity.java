package com.example.ijmsb.trainingapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

public class DisplayMessageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        TextView textView = findViewById(R.id.textView);
        textView.setText(message);
        SharedPreferences.Editor editor = getSharedPreferences("MyPreff", MODE_PRIVATE).edit();
        editor.putString("email", "1.sd.co");
        editor.apply();
    }
    @Override
    public void onBackPressed()
    {
        this.finish();
        overridePendingTransition  (R.anim.right_slide_in, R.anim.right_slide_out);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        this.finish();
        overridePendingTransition  (R.anim.right_slide_in, R.anim.right_slide_out);
        return true;
    }
}
