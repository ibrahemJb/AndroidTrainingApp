package com.example.ijmsb.trainingapp;

import org.json.JSONException;
import org.json.JSONObject;

public class ListItem {
    private String head;
    private String imageUrl;
    private JSONObject jsonObject;
    private String time;
    private int Id;
    public ListItem(JSONObject object) {
        try{
            this.head = object.get("Title").toString();
            this.imageUrl = object.get("img").toString();
            this.jsonObject = object;
            this.Id = object.getInt("ID");
            this.time = object.get("time").toString();
        }catch (JSONException je){
            je.printStackTrace();
        }
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public String getHead() {
        return head;
    }

    public String getImageUrl() {
        return imageUrl;
    }
    public String getTime(){
        return  time;
    }
    public int getId(){
        return Id;
    }
}
