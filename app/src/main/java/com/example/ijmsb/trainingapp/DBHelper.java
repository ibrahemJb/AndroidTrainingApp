package com.example.ijmsb.trainingapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "TrainingApp.db";
    private static final int DATABASE_VERSION = 1;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME , null, 1);
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table IF NOT EXISTS Favourite " +
                        "(id integer primary key, animeObject text)"
        );
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS Favourite");
        onCreate(db);
    }
    public boolean insertFavourite(int id, String animeObject){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id",id);
        contentValues.put("animeObject",animeObject);
        db.insert("Favourite", null, contentValues);
        db.close();
        return  true;
    }
    public  boolean updateFavourite(int id,String animeObject){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id",id);
        contentValues.put("animeObject",animeObject);
        db.update("Favourite", contentValues, "id="+id, null);
        return true;
    }
    public Cursor getData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from Favourite where id="+id+"", null );
        if (res != null)
            res.moveToFirst();
        res.close();
        return res;
    }
    public void deleteFavourite (Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("Favourite",
                "id = ? ",
                new String[] { Integer.toString(id) });
        db.close();
    }
    public ArrayList<String> getAllFavourites() {
        ArrayList<String> array_list = new ArrayList<String>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from Favourite", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            array_list.add(res.getString(res.getColumnIndex("animeObject")));
            res.moveToNext();
        }
        db.close();
        return array_list;
    }
}