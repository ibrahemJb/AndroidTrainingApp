package com.example.ijmsb.trainingapp;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class NewsActivity extends AppCompatActivity {
    private TextView Title,Text;
    private ImageView imageView;
    private Button button;
    private JSONObject jsonObject;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        Intent intent = getIntent();
        String stringObj = intent.getStringExtra("Object");
        Title = (TextView) findViewById(R.id.textViewTitle);
        Text = (TextView) findViewById(R.id.TextViewText);
        imageView = (ImageView) findViewById(R.id.newsImageView);
        button = (Button) findViewById(R.id.NewsVideoButton);
        try{
            JSONObject obj = new JSONObject(stringObj);
            getSupportActionBar().setTitle(obj.get("title").toString());
            if(obj.get("video").toString().equals("")){
                button.setVisibility(View.GONE);
            }
            jsonObject = obj;
            Picasso.get()
                    .load(obj.get("img").toString())
                    .resize(200,250)
                    .into(imageView);
            Title.setText(obj.get("title").toString());
            Text.setText(obj.get("text").toString());
        }catch (JSONException je){
            je.printStackTrace();
        }
    }
    @Override
    public void onBackPressed()
    {
        this.finish();
        overridePendingTransition  (R.anim.right_slide_in, R.anim.right_slide_out);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        this.finish();
        overridePendingTransition  (R.anim.right_slide_in, R.anim.right_slide_out);
        return true;
    }
    public void openVideo(View view){
        try{
            String videoUrl = jsonObject.getString("video").toString();
            if(videoUrl.equals("") == false){
                Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(videoUrl));
                startActivity(intent);
            }
        }catch (JSONException je){
            je.printStackTrace();
        }

    }
}
